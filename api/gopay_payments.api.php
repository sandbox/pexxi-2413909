<?php
/**
 * Trida obsahuje kody pro jednotlive staty
 */
class CountryCode {
  const ABW = "ABW"; //Aruba
  const AFG = "AFG"; //Afghanistan
  const AGO = "AGO"; //Angola
  const AIA = "AIA"; //Anguilla
  const ALA = "ALA"; //Åland Islands
  const ALB = "ALB"; //Albania
  const ARE = "ARE"; //United Arab Emirates
  const ARG = "ARG"; //Argentina
  const ARM = "ARM"; //Armenia
  const ASM = "ASM"; //American Samoa
  const ATA = "ATA"; //Antarctica
  const ATF = "ATF"; //French Southern Territories
  const ATG = "ATG"; //Antigua and Barbuda
  const AUS = "AUS"; //Australia
  const AUT = "AUT"; //Austria
  const AZE = "AZE"; //Azerbaijan
  const BDI = "BDI"; //Burundi
  const BEL = "BEL"; //Belgium
  const BEN = "BEN"; //Benin
  const BES = "BES"; //Bonaire, Sint Eustatius and Saba
  const BFA = "BFA"; //Burkina Faso
  const BGD = "BGD"; //Bangladesh
  const BGR = "BGR"; //Bulgaria
  const BHR = "BHR"; //Bahrain
  const BHS = "BHS"; //Bahamas
  const BIH = "BIH"; //Bosnia and Herzegovina
  const BLM = "BLM"; //Saint Barthélemy
  const BLR = "BLR"; //Belarus
  const BLZ = "BLZ"; //Belize
  const BMU = "BMU"; //Bermuda
  const BOL = "BOL"; //Bolivia, Plurinational State of
  const BRA = "BRA"; //Brazil
  const BRB = "BRB"; //Barbados
  const BRN = "BRN"; //Brunei Darussalam
  const BTN = "BTN"; //Bhutan
  const BVT = "BVT"; //Bouvet Island
  const BWA = "BWA"; //Botswana
  const CAF = "CAF"; //Central African Republic
  const CAN = "CAN"; //Canada
  const CCK = "CCK"; //Cocos (Keeling) Islands
  const CHE = "CHE"; //Switzerland
  const CHL = "CHL"; //Chile
  const CHN = "CHN"; //China
  const CIV = "CIV"; //Côte d'Ivoire
  const CMR = "CMR"; //Cameroon
  const COD = "COD"; //Congo, the Democratic Republic of the
  const COG = "COG"; //Congo
  const COK = "COK"; //Cook Islands
  const COL = "COL"; //Colombia
  const COM = "COM"; //Comoros
  const CPV = "CPV"; //Cape Verde
  const CRI = "CRI"; //Costa Rica
  const CUB = "CUB"; //Cuba
  const CUW = "CUW"; //Curaçao
  const CXR = "CXR"; //Christmas Island
  const CYM = "CYM"; //Cayman Islands
  const CYP = "CYP"; //Cyprus
  const CZE = "CZE"; //Czech Republic
  const DEU = "DEU"; //Germany
  const DJI = "DJI"; //Djibouti
  const DMA = "DMA"; //Dominica
  const DNK = "DNK"; //Denmark
  const DOM = "DOM"; //Dominican Republic
  const DZA = "DZA"; //Algeria
  const ECU = "ECU"; //Ecuador
  const EGY = "EGY"; //Egypt
  const ERI = "ERI"; //Eritrea
  const ESH = "ESH"; //Western Sahara
  const ESP = "ESP"; //Spain
  const EST = "EST"; //Estonia
  const ETH = "ETH"; //Ethiopia
  const FIN = "FIN"; //Finland
  const FJI = "FJI"; //Fiji
  const FLK = "FLK"; //Falkland Islands (Malvinas)
  const FRA = "FRA"; //France
  const FRO = "FRO"; //Faroe Islands
  const FSM = "FSM"; //Micronesia, Federated States of
  const GAB = "GAB"; //Gabon
  const GBR = "GBR"; //United Kingdom
  const GEO = "GEO"; //Georgia
  const GGY = "GGY"; //Guernsey
  const GHA = "GHA"; //Ghana
  const GIB = "GIB"; //Gibraltar
  const GIN = "GIN"; //Guinea
  const GLP = "GLP"; //Guadeloupe
  const GMB = "GMB"; //Gambia
  const GNB = "GNB"; //Guinea-Bissau
  const GNQ = "GNQ"; //Equatorial Guinea
  const GRC = "GRC"; //Greece
  const GRD = "GRD"; //Grenada
  const GRL = "GRL"; //Greenland
  const GTM = "GTM"; //Guatemala
  const GUF = "GUF"; //French Guiana
  const GUM = "GUM"; //Guam
  const GUY = "GUY"; //Guyana
  const HKG = "HKG"; //Hong Kong
  const HMD = "HMD"; //Heard Island and McDonald Islands
  const HND = "HND"; //Honduras
  const HRV = "HRV"; //Croatia
  const HTI = "HTI"; //Haiti
  const HUN = "HUN"; //Hungary
  const IDN = "IDN"; //Indonesia
  const IMN = "IMN"; //Isle of Man
  const IND = "IND"; //India
  const IOT = "IOT"; //British Indian Ocean Territory
  const IRL = "IRL"; //Ireland
  const IRN = "IRN"; //Iran, Islamic Republic of
  const IRQ = "IRQ"; //Iraq
  const ISL = "ISL"; //Iceland
  const ISR = "ISR"; //Israel
  const ITA = "ITA"; //Italy
  const JAM = "JAM"; //Jamaica
  const JEY = "JEY"; //Jersey
  const JOR = "JOR"; //Jordan
  const JPN = "JPN"; //Japan
  const KAZ = "KAZ"; //Kazakhstan
  const KEN = "KEN"; //Kenya
  const KGZ = "KGZ"; //Kyrgyzstan
  const KHM = "KHM"; //Cambodia
  const KIR = "KIR"; //Kiribati
  const KNA = "KNA"; //Saint Kitts and Nevis
  const KOR = "KOR"; //Korea, Republic of
  const KWT = "KWT"; //Kuwait
  const LAO = "LAO"; //Lao People's Democratic Republic
  const LBN = "LBN"; //Lebanon
  const LBR = "LBR"; //Liberia
  const LBY = "LBY"; //Libyan Arab Jamahiriya
  const LCA = "LCA"; //Saint Lucia
  const LIE = "LIE"; //Liechtenstein
  const LKA = "LKA"; //Sri Lanka
  const LSO = "LSO"; //Lesotho
  const LTU = "LTU"; //Lithuania
  const LUX = "LUX"; //Luxembourg
  const LVA = "LVA"; //Latvia
  const MAC = "MAC"; //Macao
  const MAF = "MAF"; //Saint Martin (French part)
  const MAR = "MAR"; //Morocco
  const MCO = "MCO"; //Monaco
  const MDA = "MDA"; //Moldova, Republic of
  const MDG = "MDG"; //Madagascar
  const MDV = "MDV"; //Maldives
  const MEX = "MEX"; //Mexico
  const MHL = "MHL"; //Marshall Islands
  const MKD = "MKD"; //Macedonia, the former Yugoslav Republic of
  const MLI = "MLI"; //Mali
  const MLT = "MLT"; //Malta
  const MMR = "MMR"; //Myanmar
  const MNE = "MNE"; //Montenegro
  const MNG = "MNG"; //Mongolia
  const MNP = "MNP"; //Northern Mariana Islands
  const MOZ = "MOZ"; //Mozambique
  const MRT = "MRT"; //Mauritania
  const MSR = "MSR"; //Montserrat
  const MTQ = "MTQ"; //Martinique
  const MUS = "MUS"; //Mauritius
  const MWI = "MWI"; //Malawi
  const MYS = "MYS"; //Malaysia
  const MYT = "MYT"; //Mayotte
  const NAM = "NAM"; //Namibia
  const NCL = "NCL"; //New Caledonia
  const NER = "NER"; //Niger
  const NFK = "NFK"; //Norfolk Island
  const NGA = "NGA"; //Nigeria
  const NIC = "NIC"; //Nicaragua
  const NIU = "NIU"; //Niue
  const NLD = "NLD"; //Netherlands
  const NOR = "NOR"; //Norway
  const NPL = "NPL"; //Nepal
  const NRU = "NRU"; //Nauru
  const NZL = "NZL"; //New Zealand
  const OMN = "OMN"; //Oman
  const PAK = "PAK"; //Pakistan
  const PAN = "PAN"; //Panama
  const PCN = "PCN"; //Pitcairn
  const PER = "PER"; //Peru
  const PHL = "PHL"; //Philippines
  const PLW = "PLW"; //Palau
  const PNG = "PNG"; //Papua New Guinea
  const POL = "POL"; //Poland
  const PRI = "PRI"; //Puerto Rico
  const PRK = "PRK"; //Korea, Democratic People's Republic of
  const PRT = "PRT"; //Portugal
  const PRY = "PRY"; //Paraguay
  const PSE = "PSE"; //Palestinian Territory, Occupied
  const PYF = "PYF"; //French Polynesia
  const QAT = "QAT"; //Qatar
  const REU = "REU"; //Réunion
  const ROU = "ROU"; //Romania
  const RUS = "RUS"; //Russian Federation
  const RWA = "RWA"; //Rwanda
  const SAU = "SAU"; //Saudi Arabia
  const SDN = "SDN"; //Sudan
  const SEN = "SEN"; //Senegal
  const SGP = "SGP"; //Singapore
  const SGS = "SGS"; //South Georgia and the South Sandwich Islands
  const SHN = "SHN"; //Saint Helena, Ascension and Tristan da Cunha
  const SJM = "SJM"; //Svalbard and Jan Mayen
  const SLB = "SLB"; //Solomon Islands
  const SLE = "SLE"; //Sierra Leone
  const SLV = "SLV"; //El Salvador
  const SMR = "SMR"; //San Marino
  const SOM = "SOM"; //Somalia
  const SPM = "SPM"; //Saint Pierre and Miquelon
  const SRB = "SRB"; //Serbia
  const SSD = "SSD"; //South Sudan
  const STP = "STP"; //Sao Tome and Principe
  const SUR = "SUR"; //Suriname
  const SVK = "SVK"; //Slovakia
  const SVN = "SVN"; //Slovenia
  const SWE = "SWE"; //Sweden
  const SWZ = "SWZ"; //Swaziland
  const SXM = "SXM"; //Sint Maarten (Dutch part)
  const SYC = "SYC"; //Seychelles
  const SYR = "SYR"; //Syrian Arab Republic
  const TCA = "TCA"; //Turks and Caicos Islands
  const TCD = "TCD"; //Chad
  const TGO = "TGO"; //Togo
  const THA = "THA"; //Thailand
  const TJK = "TJK"; //Tajikistan
  const TKL = "TKL"; //Tokelau
  const TKM = "TKM"; //Turkmenistan
  const TLS = "TLS"; //Timor-Leste
  const TON = "TON"; //Tonga
  const TTO = "TTO"; //Trinidad and Tobago
  const TUN = "TUN"; //Tunisia
  const TUR = "TUR"; //Turkey
  const TUV = "TUV"; //Tuvalu
  const TWN = "TWN"; //Taiwan, Province of China
  const TZA = "TZA"; //Tanzania, United Republic of
  const UGA = "UGA"; //Uganda
  const UKR = "UKR"; //Ukraine
  const UMI = "UMI"; //United States Minor Outlying Islands
  const URY = "URY"; //Uruguay
  const USA = "USA"; //United States
  const UZB = "UZB"; //Uzbekistan
  const VAT = "VAT"; //Holy See (Vatican City State)
  const VCT = "VCT"; //Saint Vincent and the Grenadines
  const VEN = "VEN"; //Venezuela, Bolivarian Republic of
  const VGB = "VGB"; //Virgin Islands, British
  const VIR = "VIR"; //Virgin Islands, U.S.
  const VNM = "VNM"; //Viet Nam
  const VUT = "VUT"; //Vanuatu
  const WLF = "WLF"; //Wallis and Futuna
  const WSM = "WSM"; //Samoa
  const YEM = "YEM"; //Yemen
  const ZAF = "ZAF"; //South Africa
  const ZMB = "ZMB"; //Zambia
  const ZWE = "ZWE"; //Zimbabwe 
}


class GopayConfig {
  
  /**
   *  Konfiguracni trida pro ziskavani URL pro praci s platbami
   *
   */
  const TEST = "TEST";
  const PROD = "PROD";
  
  /**
   * Parametr specifikujici, pracuje-li se na testovacim ci provoznim prostredi
   */
  static $version = self::TEST;
  
  /**
   * Nastaveni testovaciho ci provozniho prostredi prostrednictvim parametru
   *
   * @param $new_version
   * TEST - Testovaci prostredi
   * PROD - Provozni prostredi
   *
   */
  public static function init($new_version) {
    self::$version = $new_version;
  }
  
  /**
   * URL platebni brany pro uplnou integraci
   *
   * @return URL
   */
  public static function integrationURL() {
    
    if (self::$version == self::PROD) {
      return 'https://gate.gopay.cz/zaplatit-plna-integrace';
    } else {
      return 'https://testgw.gopay.cz/zaplatit-plna-integrace';
    }
  }
  
  /**
   * URL webove sluzby GoPay
   *
   * @return URL - wsdl
   */
  public static function ws() {
    
    if (self::$version == self::PROD) {
      return 'https://gate.gopay.cz/axis/EPaymentService?wsdl';
    } else {
      return 'https://testgw.gopay.cz/axis/EPaymentService?wsdl';
    }
  }
  
  
  /**
   * URL platebni brany pro zakladni integraci
   *
   * @return URL
   */
  public static function baseIntegrationURL() {
    
    if (self::$version == self::PROD) {
      return 'https://gate.gopay.cz/zaplatit-jednoducha-integrace';
    } else {
      return 'https://testgw.gopay.cz/zaplatit-jednoducha-integrace';
    }
  }
  
  /**
   * URL platebni brany pro zakladni integraci
   *
   * @return URL
   */
  public static function httpPaymentURL() {
    
    if (self::$version == self::PROD) {
      return 'https://gate.gopay.cz/vytvorit-platbu';
    } else {
      return 'https://testgw.gopay.cz/vytvorit-platbu';
    }
  }
  
  /**
   * URL platebni brany pro zakladni integraci
   *
   * @return URL
   */
  public static function httpStatusURL() {
    
    if (self::$version == self::PROD) {
      return 'https://gate.gopay.cz/stav-platby-gw2';
    } else {
      return 'https://testgw.gopay.cz/stav-platby-gw2';
    }
  }
  
  /**
   * URL platebni brany pro platebni tlacitko
   *
   * @return URL
   */
  public static function buyerBaseIntegrationURL() {
    
    if (self::$version == self::PROD) {
      return 'https://gate.gopay.cz/zaplatit-tlacitko';
    } else {
      return 'https://testgw.gopay.cz/zaplatit-tlacitko';
    }
  }
  
}



/**
 * Předpokladem je PHP verze 5.1.2 a vyšší s modulem mcrypt.
 *
 * Pomocna trida pro platbu v systemu GoPay
 *
 * - sestavovani retezcu pro podpis komunikacnich elementu
 * - sifrovani/desifrovani retezcu
 * - verifikace podpisu informacnich retezcu
 */
class GopayHelper {
  
  public static function getResultMessage($result) {
    
    $resultMesages = array(
      "PAYMENT_DONE" => "Platba byla úspěšně provedena.<br>Děkujeme Vám za využití našich služeb.",
      "CANCELED" => "Platba byla zrušena.<br>Opakujte platbu znovu, prosím.",
      "TIMEOUTED" => "Platba byla zrušena.<br>Opakujte platbu znovu, prosím.",
      "WAITING" => "Platba zatím nebyla provedena. O provedení platby Vás budeme neprodleně informovat pomocí emailu s potvrzením platby. Pokud neobdržíte do následujícího pracovního dne potvrzovací email o platbě, kontaktujte podporu GoPay na emailu podpora@gopay.cz.",
      "WAITING_OFFLINE" => "Platba zatím nebyla provedena. Na platební bráně GoPay jste získali platební údaje a na Váš email Vám byly zaslány informace k provedení platby. O provedení platby Vás budeme budeme neprodleně informovat pomocí emailu s potvrzením platby.",
      "FAILED" => "V průběhu platby nastala chyba. Kontaktujte podporu GoPay na emailu podpora@gopay.cz."
    );
    
    return isset($resultMesages[$result]) ? $resultMesages[$result] : "";
  }
  
  const iconRychloplatba = "https://www.gopay.cz/download/PT_rychloplatba.png";
  const iconDaruj = "https://www.gopay.cz/download/PT_daruj.png";
  const iconBuynow = "https://www.gopay.cz/download/PT_buynow.png";
  const iconDonate = "https://www.gopay.cz/download/PT_donate.png";
  /*
   * Kody stavu platby
   */
  const PAYMENT_DONE = "PAYMENT_DONE";
  const CANCELED = "CANCELED";
  const TIMEOUTED = "TIMEOUTED";
  const WAITING = "WAITING";
  const FAILED = "FAILED";
  
  /**
   * Sestaveni retezce pro podpis platebniho prikazu.
   *
   * @param float $goId - identifikator eshopu / uzivateli prideleny GoPay
   * @param string $productName - popis objednavky zobrazujici se na platebni brane
   * @param float $totalPriceInCents - celkova cena objednavky v halerich
   * @param string $variableSymbol - identifikator objednavky v eshopu
   * @param string $failedURL - URL stranky, kam je zakaznik presmerovan po zruseni platby / neuspesnem zaplaceni
   * @param string $successURL - URL stranky, kam je zakaznik presmerovan po uspesnem zaplaceni
   * @param string $secret - kryptovaci heslo pridelene eshopu / uzivateli, urcene k podepisovani komunikace
   * @return retezec pro podpis - platebni kanaly, ktere se zobrazi na plat. brane
   */
  public static function concatPaymentCommand($goId, $productName, $totalPriceInCents, $variableSymbol, $failedURL, $successURL, $secret) {
    return $goId . "|" . trim($productName) . "|" . $totalPriceInCents . "|" . trim($variableSymbol) . "|" . trim($failedURL) . "|" . trim($successURL) . "|" . $secret;
  }
  
  /**
   * Sestaveni retezce pro podpis vysledku vytvoreni platby
   *
   * @param float $goId - identifikator eshopu / uzivateli prideleny GoPay
   * @param string $productName - popis objednavky zobrazujici se na platebni brane
   * @param float $totalPriceInCents - celkova cena objednavky v halerich
   * @param string $variableSymbol - identifikator objednavky v eshopu
   * @param string $result - vysledek volani (CALL_COMPLETED / CALL_FAILED)
   * @param string $sessionState - stav platby (PAYMENT_DONE, WAITING, TIMEOUTED, CANCELED)
   * @param string $secret - kryptovaci heslo pridelene eshopu / uzivateli, urcene k podepisovani komunikace
   * @return retezec pro podpis
   */
  public static function concatPaymentResult($goId, $productName, $totalPriceInCents, $variableSymbol, $result, $sessionState, $secret) {
    return $goId . "|" . trim($productName) . "|" . $totalPriceInCents . "|" . trim($variableSymbol) . "|" . $result . "|" . $sessionState . "|" . $secret;
  }
  
  /**
   * Sestaveni retezce pro podpis vysledku stav platby.
   *
   * @param float $goId - identifikator eshopu / uzivateli prideleny GoPay
   * @param string $productName - popis objednavky zobrazujici se na platebni brane
   * @param float $totalPriceInCents - celkova cena objednavky v halerich
   * @param string $variableSymbol - identifikator objednavky v eshopu
   * @param string $result - vysledek volani (CALL_COMPLETED / CALL_FAILED)
   * @param string $sessionState - stav platby (PAYMENT_DONE, WAITING, TIMEOUTED, CANCELED)
   * @param string $paymentChannel - pouzita platebni metoda
   * @param string $secret - kryptovaci heslo pridelene eshopu / uzivateli, urcene k podepisovani komunikace
   * @return retezec pro podpis
   */
  public static function concatPaymentStatus($goId, $productName, $totalPriceInCents, $variableSymbol, $result, $sessionState, $paymentChannel, $secret) {
    return $goId . "|" . trim($productName) . "|" . $totalPriceInCents . "|" . trim($variableSymbol) . "|" . $result . "|" . $sessionState . "|" . $paymentChannel . "|" . $secret;
  }
  
  /**
   * Sestaveni retezce pro podpis platební session pro přesměrování na platební bránu GoPay
   * nebo volání GoPay služby stav platby
   *
   * @param float $goId - identifikator eshopu / uzivateli prideleny GoPay
   * @param float $paymentSessionId - identifikator platby na GoPay
   * @param string $secret - kryptovaci heslo pridelene eshopu / uzivateli, urcene k podepisovani komunikace
   * @return retezec pro podpis
   */
  public static function concatPaymentSession($goId, $paymentSessionId, $secret) {
    return $goId . "|" . $paymentSessionId . "|" . $secret;
  }
  
  /**
   * Sestaveni retezce pro podpis parametru platby (paymentIdentity)
   *
   * @param float $goId - identifikator eshopu / uzivateli prideleny GoPay
   * @param float $paymentSessionId - identifikator platby na GoPay
   * @param string $variableSymbol - identifikator platby na eshopu
   * @param string $secret - kryptovaci heslo pridelene eshopu / uzivateli, urcene k podepisovani komunikace
   * @return retezec pro podpis
   */
  public static function concatPaymentIdentity($goId, $paymentSessionId, $variableSymbol, $secret) {
    return $goId . "|" . $paymentSessionId . "|" . trim($variableSymbol) . "|" . $secret;
  }
  
  /**
   * Sestaveni retezce pro podpis vytvoreni uzivatele
   *
   * @param float $goId - identifikator eshopu / uzivateli prideleny GoPay
   * @param float $buyerUsername - uzivatelske jmeno uzivatele
   * @param string $buyerEmail - email uzivatele
   * @param string $secret - kryptovaci heslo pridelene uzivateli, urcene k podepisovani komunikace
   * @return retezec pro podpis
   */
  public static function concatBuyer($goId, $buyerUsername, $buyerEmail, $secret) {
    return $goId . "|" . trim($buyerUsername) . "|" . trim($buyerEmail) . "|" . $secret;
  }
  
  /**
   * Sifrovani dat 3DES
   *
   * @param string $data - retezec, ktery se sifruje
   * @param string $secret - kryptovaci heslo pridelene eshopu / uzivateli, urcene k podepisovani komunikace
   * @return sifrovany obsah v HEX forme
   */
  public static function encrypt($data, $secret) {
    $td        = mcrypt_module_open(MCRYPT_3DES, '', MCRYPT_MODE_ECB, '');
    $mcrypt_iv = mcrypt_create_iv(mcrypt_enc_get_iv_size($td), MCRYPT_RAND);
    mcrypt_generic_init($td, substr($secret, 0, mcrypt_enc_get_key_size($td)), $mcrypt_iv);
    $encrypted_data = mcrypt_generic($td, $data);
    mcrypt_generic_deinit($td);
    mcrypt_module_close($td);
    
    return bin2hex($encrypted_data);
  }
  
  /**
   * Desifrovani
   *
   * @param string $data - zasifrovana data
   * @param string $secret - kryptovaci heslo pridelene eshopu / uzivateli, urcene k podepisovani komunikace
   * @return desifrovany retezec
   */
  public static function decrypt($data, $secret) {
    $td        = mcrypt_module_open(MCRYPT_3DES, '', MCRYPT_MODE_ECB, '');
    $mcrypt_iv = mcrypt_create_iv(mcrypt_enc_get_iv_size($td), MCRYPT_RAND);
    mcrypt_generic_init($td, substr($secret, 0, mcrypt_enc_get_key_size($td)), $mcrypt_iv);
    
    $decrypted_data = mdecrypt_generic($td, GopayHelper::convert($data));
    mcrypt_generic_deinit($td);
    mcrypt_module_close($td);
    
    return Trim($decrypted_data);
  }
  
  /**
   * Hash SHA1 dat
   *
   * @param string $data - data k hashovani
   * @return otisk dat SHA1
   */
  public static function hash($data) {
    $hash = '';
    
    if (function_exists("sha1") == true) {
      $hash = sha1($data, true);
    } else {
      $hash = mhash(MHASH_SHA1, $data);
    }
    
    return bin2hex($hash);
  }
  
  /**
   * Konverze z HEX -> string
   *
   * @param string $hexString - data k konverzi
   * @return konverze z HEX -> string
   */
  public static function convert($hexString) {
    
    $hexLenght = strlen($hexString);
    // only hex numbers is allowed
    if ($hexLenght % 2 != 0 || preg_match("/[^\da-fA-F]/", $hexString)) {
      return FALSE;
    }
    $binString = "";
    for ($x = 1; $x <= $hexLenght / 2; $x++) {
      $binString .= chr(hexdec(substr($hexString, 2 * $x - 2, 2)));
    }
    
    return $binString;
  }
  
  /**
   * Kontrola vysledku vytvoreni platby proti internim udajum objednavky - verifikace podpisu.
   *
   * @param mixed $payment_result - vysledek volani createPayment
   * @param string $session_state - ocekavany stav paymentSession (WAITING, PAYMENT_DONE)
   * @param float $eshopGoId - identifikace eshopu - GoId eshopu pridelene GoPay
   * @param string $variableSymbol - identifikace akt. objednavky na eshopu
   * @param float $totalPriceInCents - cena objednavky v halerich
   * @param string $productName - nazev objedavky / zbozi
   * @param string $secret - kryptovaci heslo pridelene eshopu, urcene k podepisovani komunikace
   *
   * @return true
   * @return false
   */
  public static function checkEshopPaymentResult($payment_result, $session_state, $eshopGoId, $variableSymbol, $totalPriceInCents, $productName, $secret) {
    
    $valid = true;
    
    /*
     * Kontrola parametru objednavky
     */
    $valid = GopayHelper::checkPaymentResultCommon($payment_result, $session_state, null, $eshopGoId, $variableSymbol, $totalPriceInCents, $productName, $secret);
    
    if ($valid) {
      
      /*
       * Kontrola podpisu objednavky
       */
      $hashedSignature = GopayHelper::hash(GopayHelper::concatPaymentResult($payment_result->eshopGoId, $payment_result->productName, $payment_result->totalPrice, $payment_result->variableSymbol, $payment_result->result, $payment_result->sessionState, $secret));
      
      $decryptedHash = GopayHelper::decrypt($payment_result->encryptedSignature, $secret);
      
      if ($decryptedHash != $hashedSignature) {
        $valid = false;
        //              echo "PS invalid signature <br>";
      }
    }
    
    return $valid;
  }
  
  /**
   * Kontrola vysledku vytvoreni platby proti internim udajum objednavky - verifikace podpisu.
   *
   * @param mixed $payment_result - vysledek volani createPayment
   * @param string $session_state - ocekavany stav paymentSession (WAITING, PAYMENT_DONE)
   * @param float $buyerGoId - identifikace uzivatele - GoId uzivatele pridelene GoPay
   * @param string $variableSymbol - identifikace akt. objednavky
   * @param float $totalPriceInCents - cena objednavky v halerich
   * @param string $productName - nazev objednavky / zbozi
   * @param string $secret - kryptovaci heslo pridelene uzivateli, urcene k podepisovani komunikace
   *
   * @return true
   * @return false
   */
  public static function checkBuyerPaymentResult($payment_result, $session_state, $buyerGoId, $variableSymbol, $totalPriceInCents, $productName, $secret) {
    
    $valid = true;
    
    /*
     * Kontrola parametru objednavky
     */
    $valid = GopayHelper::checkPaymentResultCommon($payment_result, $session_state, $buyerGoId, null, $variableSymbol, $totalPriceInCents, $productName, $secret);
    
    if ($valid) {
      
      /*
       * Kontrola podpisu objednavky
       */
      $hashedSignature = GopayHelper::hash(GopayHelper::concatPaymentResult($payment_result->buyerGoId, $payment_result->productName, $payment_result->totalPrice, $payment_result->variableSymbol, $payment_result->result, $payment_result->sessionState, $secret));
      
      $decryptedHash = GopayHelper::decrypt($payment_result->encryptedSignature, $secret);
      
      if ($decryptedHash != $hashedSignature) {
        $valid = false;
        //              echo "PS invalid signature <br>";
      }
    }
    
    return $valid;
  }
  
  /**
   * Kontrola stavu platby proti internim udajum objednavky - verifikace podpisu.
   *
   * @param mixed $payment_status - vysledek volani paymentStatus
   * @param string $session_state - ocekavany stav paymentSession (WAITING, PAYMENT_DONE)
   * @param float $eshopGoId - identifikace eshopu - GoId eshopu pridelene GoPay
   * @param string $variableSymbol - identifikace akt. objednavky na eshopu
   * @param float $totalPriceInCents - cena objednavky v halerich
   * @param string $productName - nazev objednavky / zbozi
   * @param string $secret - kryptovaci heslo pridelene eshopu, urcene k podepisovani komunikace
   *
   * @return true
   * @return false
   */
  public static function checkEshopPaymentStatus($payment_status, $session_state, $eshopGoId, $variableSymbol, $totalPriceInCents, $productName, $secret) {
    
    $valid = true;
    
    $valid = GopayHelper::checkPaymentResultCommon($payment_status, $session_state, null, $eshopGoId, $variableSymbol, $totalPriceInCents, $productName, $secret);
    
    
    if ($valid) {
      /*
       * Kontrola podpisu objednavky
       */
      $hashedSignature = GopayHelper::hash(GopayHelper::concatPaymentStatus($payment_status->eshopGoId, $payment_status->productName, $payment_status->totalPrice, $payment_status->variableSymbol, $payment_status->result, $payment_status->sessionState, $payment_status->paymentChannel, $secret));
      
      $decryptedHash = GopayHelper::decrypt($payment_status->encryptedSignature, $secret);
      
      if ($decryptedHash != $hashedSignature) {
        $valid = false;
        //              echo "PS invalid signature <br>";
      }
    }
    
    return $valid;
  }
  
  /**
   * Kontrola stavu platby proti internim udajum objednavky uzivatele - verifikace podpisu
   *
   * @param mixed $payment_status - vysledek volani paymentStatus
   * @param string $session_state - ocekavany stav paymentSession (WAITING, PAYMENT_DONE)
   * @param float $buyerGoId - identifikace uzivatele - GoId uzivatele pridelene GoPay
   * @param string $variableSymbol - identifikace akt. objednavky
   * @param float $totalPriceInCents - cena objednavky v halerich
   * @param string $productName - nazev objednavky / zbozi
   * @param string $secret - kryptovaci heslo pridelene uzivateli, urcene k podepisovani komunikace
   *
   * @return true
   * @return false
   */
  public static function checkBuyerPaymentStatus($payment_status, $session_state, $buyerGoId, $variableSymbol, $totalPriceInCents, $productName, $secret) {
    
    $valid = true;
    
    /*
     * Kontrola parametru objednavky
     */
    $valid = GopayHelper::checkPaymentResultCommon($payment_status, $session_state, $buyerGoId, null, $variableSymbol, $totalPriceInCents, $productName, $secret);
    
    if ($valid) {
      
      /*
       * Kontrola parametru objednavky
       */
      $hashedSignature = GopayHelper::hash(GopayHelper::concatPaymentStatus($payment_status->buyerGoId, $payment_status->productName, $payment_status->totalPrice, $payment_status->variableSymbol, $payment_status->result, $payment_status->sessionState, $payment_status->paymentChannel, $secret));
      
      $decryptedHash = GopayHelper::decrypt($payment_status->encryptedSignature, $secret);
      
      if ($decryptedHash != $hashedSignature) {
        $valid = false;
        //              echo "PS invalid signature <br>";
      }
    }
    
    return $valid;
  }
  
  /**
   * Kontrola parametru platby proti internim udajum objednavky uzivatele
   *
   * @param string $payment_result - vysledek volani paymentStatus
   * @param string $session_state - ocekavany stav paymentSession (WAITING, PAYMENT_DONE)
   * @param float $buyerGoId - identifikace uzivatele - GoId eshopu / uzivatele pridelene GoPay
   * @param string $variableSymbol - identifikace akt. objednavky
   * @param float $totalPriceInCents - cena objednavky v halerich
   * @param string $productName - nazev zbozi / zbozi
   * @param string $secret - kryptovaci heslo pridelene eshopu / uzivateli, urcene k podepisovani komunikace
   *
   * @return true
   * @return false
   */
  private static function checkPaymentResultCommon($payment_result, $session_state, $buyerGoId, $eshopGoId, $variableSymbol, $totalPriceInCents, $productName, $secret) {
    
    $valid = true;
    
    if ($payment_result) {
      
      if ($payment_result->result != 'CALL_COMPLETED') {
        $valid = false;
        //              echo "PS invalid call state state<br>";
      }
      
      if ($payment_result->sessionState != $session_state) {
        $valid = false;
        //              echo "PS invalid session state<br>";
      }
      
      if (trim($payment_result->variableSymbol) != trim($variableSymbol)) {
        $valid = false;
        //              echo "PS invalid VS <br>";
      }
      
      if (trim($payment_result->productName) != trim($productName)) {
        $valid = false;
        //              echo "PS invalid PN <br>";
      }
      
      if ($payment_result->eshopGoId != $eshopGoId) {
        $valid = false;
        //              echo "PS invalid EID<br>";
      }
      
      if ($payment_result->buyerGoId != $buyerGoId) {
        $valid = false;
        //              echo "PS invalid EID<br>";
      }
      
      if ($payment_result->totalPrice != $totalPriceInCents) {
        $valid = false;
        //              echo "PS invalid price<br>";
      }
    } else {
      $valid = false;
      //          echo "none payment status <br>";
    }
    
    return $valid;
  }
  
  /**
   * Kontrola parametru predavanych ve zpetnem volani po potvrzeni/zruseni platby - verifikace podpisu.
   *
   * @param float $returnedGoId - goId vracene v redirectu
   * @param float $returnedPaymentSessionId - paymentSessionId vracene v redirectu
   * @param string $returnedVariableSymbol - variableSymbol vraceny v redirectu - identifikator platby na eshopu
   * @param string $returnedEncryptedSignature - kontrolni podpis vraceny v redirectu
   * @param float $goId - identifikace eshopu - GoId eshopu pridelene GoPay
   * @param string $variableSymbol - identifikace akt. objednavky
   * @param string $secret - kryptovaci heslo pridelene eshopu / uzivateli, urcene k podepisovani komunikace
   *
   * @return true
   * @return false
   */
  public static function checkPaymentIdentity($returnedGoId, $returnedPaymentSessionId, $returnedVariableSymbol, $returnedEncryptedSignature, $goId, $variableSymbol, $secret) {
    
    $valid = true;
    if (trim($returnedVariableSymbol) != trim($variableSymbol)) {
      $valid = false;
      //          echo "PI invalid VS <br>";
    }
    
    if ($returnedGoId != $goId) {
      $valid = false;
      //          echo "PI invalid EID<br>";
    }
    
    $hashedSignature = GopayHelper::hash(GopayHelper::concatPaymentIdentity((float) $returnedGoId, (float) $returnedPaymentSessionId, $returnedVariableSymbol, $secret));
    $decryptedHash   = GopayHelper::decrypt($returnedEncryptedSignature, $secret);
    
    if ($decryptedHash != $hashedSignature) {
      $valid = false;
      //          echo "PI invalid signature <br>";
    }
    
    return $valid;
  }
  
  /**
   * Kontrola parametru predavanych ve zpetnem volani po vytvoreni uzivatele - verifikace podpisu.
   *
   * @param mixed $create_result - vysledek volani createBuyer
   * @param float $goId - identifikace uzivatele - GoId uzivatele pridelene GoPay
   * @param string $buyerUsername - uzivatelske jmeno uzivatele
   * @param string $buyerEmail - email uzivatele
   * @param string $secret - kryptovaci heslo pridelene uzivateli, urcene k podepisovani komunikace
   *
   * @return true
   * @return false
   */
  public static function checkCreateBuyerResult($create_result, $goId, $buyerUsername, $buyerEmail, $secret) {
    
    $valid = true;
    
    if ($create_result) {
      
      if ($create_result->buyerGoId == "") {
        $valid = false;
        //              echo "PS invalid buyerGoId<br>";
      }
      
      if ($create_result->buyerUsername == "") {
        $valid = false;
        //              echo "PS invalid buyerUsername<br>";
      }
      
      if ($create_result->result != 'CALL_COMPLETED') {
        $valid = false;
        //              echo "PS invalid call state state<br>";
      }
      
      if ($create_result->resultDescription != 'BUYER_CREATED') {
        $valid = false;
        //              echo "PS invalid call state description<br>";
      }
      
      if ($valid) {
        
        $hashedSignature = GopayHelper::hash(GopayHelper::concatBuyer((float) $goId, $buyerUsername, $buyerEmail, $secret));
        
        $decryptedHash = GopayHelper::decrypt($create_result->encryptedSignature, $secret);
        
        if ($decryptedHash != $hashedSignature) {
          $valid = false;
          //                  echo "PS invalid signature <br>";
        }
      }
    } else {
      $valid = false;
      //          echo "No create result <br>";
    }
    
    return $valid;
  }
  
  //  ---      ESHOP   ----
  
  /**
   * Sestaveni formulare platebniho tlacitka pro jednoduchou integraci
   *
   *
   * @param float $eshopGoId - identifikace eshopu - GoId eshopu pridelene GoPay
   * @param int $totalPrice - cena objednavky v halerich
   * @param string $productName - nazev objednavky / zbozi
   * @param string $variableSymbol - identifikace akt. objednavky na eshopu
   * @param string $successURL - URL, kam se ma prejit po uspesnem zaplaceni
   * @param string $failedURL - URL, kam se ma prejit po neuspesnem zaplaceni
   * @param string $secret - kryptovaci heslo pridelene eshopu, urcene k podepisovani komunikace
   * @param array $paymentChannels - pole plat. metod, ktere se zobrazi na brane
   *
   * @return HTML kod platebniho formulare
   */
  public static function createEshopForm($eshopGoId, $totalPrice, $productName, $variableSymbol, $successURL, $failedURL, $secret, $paymentChannels) {
    
    $encryptedSignature = GopayHelper::encrypt(GopayHelper::hash(GopayHelper::concatPaymentCommand((float) $eshopGoId, $productName, (int) $totalPrice, $variableSymbol, $failedURL, $successURL, $secret)), $secret);
    
    $ouput = "";
    $ouput .= "<form method='post' action='" . GopayConfig::baseIntegrationURL() . "'>\n";
    $ouput .= '<input type="hidden" name="paymentCommand.eshopGoId" value="' . $eshopGoId . '" />' . "\n";
    $ouput .= '<input type="hidden" name="paymentCommand.productName" value="' . trim($productName) . '" />' . "\n";
    $ouput .= '<input type="hidden" name="paymentCommand.totalPrice" value="' . $totalPrice . '" />' . "\n";
    $ouput .= '<input type="hidden" name="paymentCommand.variableSymbol" value="' . trim($variableSymbol) . '"/>' . "\n";
    $ouput .= '<input type="hidden" name="paymentCommand.successURL" value="' . trim($successURL) . '" />' . "\n";
    $ouput .= '<input type="hidden" name="paymentCommand.failedURL" value="' . trim($failedURL) . '" />' . "\n";
    $ouput .= '<input type="hidden" name="paymentCommand.paymentChannels" value="' . join($paymentChannels, ",") . '" />' . "\n";
    $ouput .= '<input type="hidden" name="paymentCommand.encryptedSignature" value="' . $encryptedSignature . '" />' . "\n";
    $ouput .= '<input type="submit" name="buy" value="Zaplatit" class="button">' . "\n";
    $ouput .= "</form>\n";
    
    return $ouput;
  }
  
  /**
   * Sestaveni platebniho tlacitka formou odkazu pro jednoduchou integraci
   *
   *
   * @param float $eshopGoId - identifikace eshopu - GoId eshopu pridelene GoPay
   * @param int $totalPrice - cena objednavky v halerich
   * @param string $productName - nazev objednavky / zbozi
   * @param string $variableSymbol - identifikace akt. objednavky na eshopu
   * @param string $successURL - URL, kam se ma prejit po uspesnem zaplaceni
   * @param string $failedURL - URL, kam se ma prejit po neuspesnem zaplaceni
   * @param string $secret - kryptovaci heslo pridelene eshopu, urcene k podepisovani komunikace
   * @param array $paymentChannels - pole plat. metod, ktere se zobrazi na brane
   *
   * @return HTML kod platebniho tlacitka
   */
  public static function createEshopHref($eshopGoId, $totalPrice, $productName, $variableSymbol, $successURL, $failedURL, $secret, $paymentChannels) {
    
    $encryptedSignature = GopayHelper::encrypt(GopayHelper::hash(GopayHelper::concatPaymentCommand((float) $eshopGoId, $productName, (int) $totalPrice, $variableSymbol, $failedURL, $successURL, $secret)), $secret);
    
    $params = "";
    $params .= "paymentCommand.eshopGoId=" . $eshopGoId;
    $params .= "&paymentCommand.productName=" . urlencode($productName);
    $params .= "&paymentCommand.totalPrice=" . $totalPrice;
    $params .= "&paymentCommand.variableSymbol=" . urlencode($variableSymbol);
    $params .= "&paymentCommand.successURL=" . urlencode($successURL);
    $params .= "&paymentCommand.failedURL=" . urlencode($failedURL);
    $params .= "&paymentCommand.paymentChannels=" . join($paymentChannels, ",");
    $params .= "&paymentCommand.encryptedSignature=" . urlencode($encryptedSignature);
    
    $ouput = "";
    $ouput .= "<a target='_blank' href='" . GopayConfig::baseIntegrationURL() . "?" . $params . "'>";
    $ouput .= " Zaplatit ";
    $ouput .= "</a>";
    
    return $ouput;
  }
  
  /**
   * Sestaveni formulare platebniho tlacitka s udaji o zakaznikovi pro jednoduchou integraci
   *
   * @param float $eshopGoId - identifikace eshopu - GoId eshopu pridelene GoPay
   * @param int $totalPrice - cena objednavky v halerich
   * @param string $productName - nazev objednavky / zbozi
   * @param string $variableSymbol - identifikace akt. objednavky na eshopu
   * @param string $successURL - URL, kam se ma prejit po uspesnem zaplaceni
   * @param string $failedURL - URL, kam se ma prejit po neuspesnem zaplaceni
   * @param string $secret - kryptovaci heslo pridelene eshopu, urcene k podepisovani komunikace
   * @param array $paymentChannels - pole plat. metod, ktere se zobrazi na brane
   *
   * Informace o zakaznikovi
   * @param string $firstName   - Jmeno zakaznika
   * @param string $lastName    - Prijmeni
   *
   * Adresa
   * @param string $city        - Mesto
   * @param string $street      - Ulice
   * @param string $postalCode  - PSC
   * @param string $countryCode - Kod zeme. Validni kody jsou uvedeny ve tride CountryCode
   * @param string $email       - Email zakaznika
   * @param string $phoneNumber - Tel. cislo
   *
   * @return HTML kod platebniho formulare
   */
  public static function createEshopFormWithCustomer($eshopGoId, $totalPrice, $productName, $variableSymbol, $successURL, $failedURL, $secret, $paymentChannels, $firstName, $lastName, $city, $street, $postalCode, $countryCode, $email, $phoneNumber) {
    
    $encryptedSignature = GopayHelper::encrypt(GopayHelper::hash(GopayHelper::concatPaymentCommand((float) $eshopGoId, $productName, (int) $totalPrice, $variableSymbol, $failedURL, $successURL, $secret)), $secret);
    
    $ouput = "";
    $ouput .= "<form method='post' action='" . GopayConfig::baseIntegrationURL() . "'>\n";
    $ouput .= '<input type="hidden" name="paymentCommand.eshopGoId" value="' . $eshopGoId . '" />' . "\n";
    $ouput .= '<input type="hidden" name="paymentCommand.productName" value="' . $productName . '" />' . "\n";
    $ouput .= '<input type="hidden" name="paymentCommand.totalPrice" value="' . $totalPrice . '" />' . "\n";
    $ouput .= '<input type="hidden" name="paymentCommand.variableSymbol" value="' . $variableSymbol . '"/>' . "\n";
    $ouput .= '<input type="hidden" name="paymentCommand.successURL" value="' . $successURL . '" />' . "\n";
    $ouput .= '<input type="hidden" name="paymentCommand.failedURL" value="' . $failedURL . '" />' . "\n";
    $ouput .= '<input type="hidden" name="paymentCommand.paymentChannels" value="' . join($paymentChannels, ",") . '" />' . "\n";
    $ouput .= '<input type="hidden" name="paymentCommand.encryptedSignature" value="' . $encryptedSignature . '" />' . "\n";
    $ouput .= '<input type="hidden" name="paymentCommand.customerData.firstName" value="' . $firstName . '" />' . "\n";
    $ouput .= '<input type="hidden" name="paymentCommand.customerData.lastName" value="' . $lastName . '" />' . "\n";
    $ouput .= '<input type="hidden" name="paymentCommand.customerData.city" value="' . $city . '" />' . "\n";
    $ouput .= '<input type="hidden" name="paymentCommand.customerData.street" value="' . $street . '" />' . "\n";
    $ouput .= '<input type="hidden" name="paymentCommand.customerData.postalCode" value="' . $postalCode . '" />' . "\n";
    $ouput .= '<input type="hidden" name="paymentCommand.customerData.countryCode" value="' . $countryCode . '" />' . "\n";
    $ouput .= '<input type="hidden" name="paymentCommand.customerData.email" value="' . $email . '" />' . "\n";
    $ouput .= '<input type="hidden" name="paymentCommand.customerData.phoneNumber" value="' . $phoneNumber . '" />' . "\n";
    $ouput .= '<input type="submit" name="buy" value="Zaplatit" class="button">' . "\n";
    $ouput .= "</form>\n";
    
    return $ouput;
  }
  
  /**
   * Sestaveni platebniho tlacitka formou odkazu s udaji o zakaznikovi pro jednoduchou integraci
   *
   * @param float $eshopGoId - identifikace eshopu - GoId eshopu pridelene GoPay
   * @param int $totalPrice - cena objednavky v halerich
   * @param string $productName - nazev objednavky / zbozi
   * @param string $variableSymbol - identifikace akt. objednavky na eshopu
   * @param string $successURL - URL, kam se ma prejit po uspesnem zaplaceni
   * @param string $failedURL - URL, kam se ma prejit po neuspesnem zaplaceni
   * @param string $secret - kryptovaci heslo pridelene eshopu, urcene k podepisovani komunikace
   * @param array $paymentChannels - pole plat. metod, ktere se zobrazi na brane
   *
   * Informace o zakaznikovi
   * @param string $firstName   - Jmeno zakaznika
   * @param string $lastName    - Prijmeni
   *
   * Adresa
   * @param string $city        - Mesto
   * @param string $street      - Ulice
   * @param string $postalCode  - PSC
   * @param string $countryCode - Kod zeme. Validni kody jsou uvedeny ve tride CountryCode
   * @param string $email       - Email zakaznika
   * @param string $phoneNumber - Tel. cislo
   *
   * @return HTML kod platebniho tlacitka
   */
  public static function createEshopHrefWithCustomer($eshopGoId, $totalPrice, $productName, $variableSymbol, $successURL, $failedURL, $secret, $paymentChannels, $firstName, $lastName, $city, $street, $postalCode, $countryCode, $email, $phoneNumber) {
    
    $encryptedSignature = GopayHelper::encrypt(GopayHelper::hash(GopayHelper::concatPaymentCommand((float) $eshopGoId, $productName, (int) $totalPrice, $variableSymbol, $failedURL, $successURL, $secret)), $secret);
    
    $params = "";
    $params .= "paymentCommand.eshopGoId=" . $eshopGoId;
    $params .= "&paymentCommand.productName=" . urlencode($productName);
    $params .= "&paymentCommand.totalPrice=" . $totalPrice;
    $params .= "&paymentCommand.variableSymbol=" . urlencode($variableSymbol);
    $params .= "&paymentCommand.successURL=" . urlencode($successURL);
    $params .= "&paymentCommand.failedURL=" . urlencode($failedURL);
    $params .= "&paymentCommand.paymentChannels=" . join($paymentChannels, ",");
    $params .= "&paymentCommand.encryptedSignature=" . urlencode($encryptedSignature);
    $params .= "&paymentCommand.customerData.firstName=" . urlencode($firstName);
    $params .= "&paymentCommand.customerData.lastName=" . urlencode($lastName);
    $params .= "&paymentCommand.customerData.city=" . urlencode($city);
    $params .= "&paymentCommand.customerData.street=" . urlencode($street);
    $params .= "&paymentCommand.customerData.postalCode=" . urlencode($postalCode);
    $params .= "&paymentCommand.customerData.countryCode=" . urlencode($countryCode);
    $params .= "&paymentCommand.customerData.email=" . urlencode($email);
    $params .= "&paymentCommand.customerData.phoneNumber=" . urlencode($phoneNumber);
    
    $ouput = "";
    $ouput .= "<a target='_blank' href='" . GopayConfig::baseIntegrationURL() . "?" . $params . "'>";
    $ouput .= " Zaplatit ";
    $ouput .= "</a>";
    
    return $ouput;
  }
  
  //  ---      UZIVATEL   ----
  
  /**
   * Sestaveni formulare platebniho tlacitka
   *
   * @param float $buyerGoId - identifikace uzivatele - GoId uzivatele pridelene GoPay
   * @param int $totalPrice - cena objednavky v halerich
   * @param string $productName - nazev objednvky / zbozi
   * @param string $variableSymbol - identifikace akt. objednavky
   * @param string $successURL - URL, kam se ma prejit po uspesnem zaplaceni
   * @param string $failedURL - URL, kam se ma prejit po neuspesnem zaplaceni
   * @param string $secret - kryptovaci heslo pridelene uzivateli, urcene k podepisovani komunikace
   * @param string $iconUrl - URL ikony tlacitka - viz konstanty tridy
   *
   * @return HTML kod platebniho formulare
   */
  public static function createBuyerForm($buyerGoId, $totalPrice, $productName, $variableSymbol, $successURL, $failedURL, $secret, $iconUrl) {
    
    $encryptedSignature = GopayHelper::encrypt(GopayHelper::hash(GopayHelper::concatPaymentCommand((float) $buyerGoId, $productName, (int) $totalPrice, $variableSymbol, $failedURL, $successURL, $secret)), $secret);
    
    $formBuffer = "";
    $formBuffer .= "<form method='post' action='" . GopayConfig::buyerBaseIntegrationURL() . "' target='_blank'>\n";
    $formBuffer .= "<input type='hidden' name='paymentCommand.buyerGoId' value='" . $buyerGoId . "' />\n";
    $formBuffer .= "<input type='hidden' name='paymentCommand.totalPrice' value='" . $totalPrice . "' />\n";
    $formBuffer .= "<input type='hidden' name='paymentCommand.productName' value='" . $productName . "' />\n";
    $formBuffer .= "<input type='hidden' name='paymentCommand.variableSymbol' value='" . $variableSymbol . "' />\n";
    $formBuffer .= "<input type='hidden' name='paymentCommand.successURL' value='" . $successURL . "' />\n";
    $formBuffer .= "<input type='hidden' name='paymentCommand.failedURL' value='" . $failedURL . "' />\n";
    $formBuffer .= "<input type='hidden' name='paymentCommand.encryptedSignature' value='" . $encryptedSignature . "' />\n";
    
    if (empty($iconUrl)) {
      $formBuffer .= "<input type='submit' name='submit' value='Zaplatit' class='button'>\n";
    } else {
      $formBuffer .= "<input type='submit' name='submit' value='' style='background:url(" . $iconUrl . ") no-repeat;width:100px;height:30px;border:none;'>\n";
    }
    
    $formBuffer .= "</form>\n";
    
    return $formBuffer;
  }
  
  /**
   * Sestaveni platebniho tlacitka jako odkazu
   *
   * @param float $buyerGoId - identifikace uzivatele - GoId uzivatele pridelene GoPay
   * @param int $totalPrice - cena objednavky v halerich
   * @param string $productName - nazev objednvky / zbozi
   * @param string $variableSymbol - identifikace akt. objednavky
   * @param string $successURL - URL, kam se ma prejit po uspesnem zaplaceni
   * @param string $failedURL - URL, kam se ma prejit po neuspesnem zaplaceni
   * @param string $secret - kryptovaci heslo pridelene uzivateli, urcene k podepisovani komunikace
   * @param string $iconUrl - URL ikony tlacitka - viz konstanty tridy
   *
   * @return HTML kod platebniho tlacitka
   */
  public static function createBuyerHref($buyerGoId, $totalPrice, $productName, $variableSymbol, $successURL, $failedURL, $secret, $iconUrl) {
    
    $encryptedSignature = GopayHelper::encrypt(GopayHelper::hash(GopayHelper::concatPaymentCommand((float) $buyerGoId, $productName, (int) $totalPrice, $variableSymbol, $failedURL, $successURL, $secret)), $secret);
    
    $params = "";
    $params .= "paymentCommand.buyerGoId=" . $buyerGoId;
    $params .= "&paymentCommand.productName=" . urlencode($productName);
    $params .= "&paymentCommand.totalPrice=" . $totalPrice;
    $params .= "&paymentCommand.variableSymbol=" . urlencode($variableSymbol);
    $params .= "&paymentCommand.successURL=" . urlencode($successURL);
    $params .= "&paymentCommand.failedURL=" . urlencode($failedURL);
    $params .= "&paymentCommand.encryptedSignature=" . urlencode($encryptedSignature);
    
    $formBuffer = "";
    $formBuffer .= "<a target='_blank' href='" . GopayConfig::buyerBaseIntegrationURL() . "?" . $params . "' >";
    
    if (empty($iconUrl)) {
      $formBuffer .= " Zaplatit ";
    } else {
      $formBuffer .= " <img src='" . $iconUrl . "' border='0' style='border:none;'/> ";
    }
    
    $formBuffer .= "</a>";
    
    return $formBuffer;
  }
  
}


/**
 * Predpokladem je PHP verze 5.1.2 a vyssi. Pro volání WS je pouzit modul soap.
 *
 * Obsahuje funkcionality pro vytvoreni platby a kontrolu stavu platby prostrednictvim WS.
 */
class GopaySoap {
  //  ---     ESHOP   ----
  
  /**
   * Vytvoreni platby pomoci WS z eshopu
   *
   * @param long $eshopGoId - identifikator eshopu - GoId
   * @param string $productName - popis objednavky zobrazujici se na platebni brane
   * @param int $totalPriceInCents - celkova cena objednavky v halerich
   * @param string $variableSymbol - identifikator objednavky v eshopu
   * @param string $successURL - URL stranky, kam je zakaznik presmerovan po uspesnem zaplaceni
   * @param string $failedURL - URL stranky, kam je zakaznik presmerovan po zruseni platby / neuspesnem zaplaceni
   * @param string $secret - kryptovaci heslo pridelene eshopu
   * @param string $paymentChannels - platebni kanaly, ktere se zobrazi na plat. brane
   *
   * @return paymentSessionId
   * @return -1 vytvoreni platby neprobehlo uspesne
   * @return -2 chyba komunikace WS
   */
  public static function createEshopPayment($eshopGoId, $productName, $totalPriceInCents, $variableSymbol, $successURL, $failedURL, $secret, $paymentChannels) {
    
    try {
      
      ini_set("soap.wsdl_cache_enabled", "0");
      $go_client = new SoapClient(GopayConfig::ws(), array());
      
      /*
       * Sestaveni pozadavku pro zalozeni platby
       */
      $encryptedSignature = GopayHelper::encrypt(GopayHelper::hash(GopayHelper::concatPaymentCommand((float) $eshopGoId, $productName, (int) $totalPriceInCents, $variableSymbol, $failedURL, $successURL, $secret)), $secret);
      
      $payment_command = array(
        "eshopGoId" => (float) $eshopGoId,
        "productName" => trim($productName),
        "totalPrice" => (int) $totalPriceInCents,
        "variableSymbol" => trim($variableSymbol),
        "successURL" => trim($successURL),
        "failedURL" => trim($failedURL),
        "encryptedSignature" => $encryptedSignature,
        "paymentChannels" => join($paymentChannels, ",")
      );
      
      /*
       * Vytvareni platby na strane GoPay prostrednictvim WS
       */
      $payment_status = $go_client->__call('createPaymentSession', array(
        'paymentCommand' => $payment_command
      ));
      
      /*
       * Kontrola stavu platby - musi byt ve stavu WAITING, kontrola parametru platby
       */
      if (GopayHelper::checkEshopPaymentResult($payment_status, 'WAITING', (float) $eshopGoId, $variableSymbol, (int) $totalPriceInCents, $productName, $secret)) {
        
        return $payment_status->paymentSessionId;
      } else {
        /*
         * Chyba pri vytvareni platby
         */
        return -1;
      }
    }
    catch (SoapFault $f) {
      /*
       * Chyba pri komunikaci s WS
       */
      return -2;
    }
  }
  
  /**
   * Vytvoreni platby s udaji o zakaznikovi pomoci WS z eshopu
   *
   * @param long $eshopGoId - identifikator eshopu - GoId
   * @param string $productName - popis objednavky zobrazujici se na platebni brane
   * @param int $totalPriceInCents - celkova cena objednavky v halerich
   * @param string $variableSymbol - identifikator objednavky v eshopu
   * @param string $successURL - URL stranky, kam je zakaznik presmerovan po uspesnem zaplaceni
   * @param string $failedURL - URL stranky, kam je zakaznik presmerovan po zruseni platby / neuspesnem zaplaceni
   * @param string $secret - kryptovaci heslo pridelene eshopu
   * @param string $paymentChannels - platebni kanaly, ktere se zobrazi na plat. brane
   *
   * Informace o zakaznikovi
   * @param string $firstName   - Jmeno zakaznika
   * @param string $lastName    - Prijmeni
   *
   * Adresa
   * @param string $city        - Mesto
   * @param string $street      - Ulice
   * @param string $postalCode  - PSC
   * @param string $countryCode - Kod zeme. Validni kody jsou uvedeny ve tride CountryCode
   * @param string $email       - Email zakaznika
   * @param string $phoneNumber - Tel. cislo
   *
   * @return paymentSessionId
   * @return -1 vytvoreni platby neprobehlo uspesne
   * @return -2 chyba komunikace WS
   */
  public static function createCustomerEshopPayment($eshopGoId, $productName, $totalPriceInCents, $variableSymbol, $successURL, $failedURL, $secret, $paymentChannels, $firstName, $lastName, $city, $street, $postalCode, $countryCode, $email, $phoneNumber) {
    try {
      ini_set("soap.wsdl_cache_enabled", "0");
      $go_client = new SoapClient(GopayConfig::ws(), array());
      
      /*
       * Sestaveni pozadavku pro zalozeni platby
       */
      $encryptedSignature = GopayHelper::encrypt(GopayHelper::hash(GopayHelper::concatPaymentCommand((float) $eshopGoId, $productName, (int) $totalPriceInCents, $variableSymbol, $failedURL, $successURL, $secret)), $secret);
      
      $customerData = array(
        "firstName" => $firstName,
        "lastName" => $lastName,
        "city" => $city,
        "street" => $street,
        "postalCode" => $postalCode,
        "countryCode" => $countryCode,
        "email" => $email,
        "phoneNumber" => $phoneNumber
      );
      
      $customerPaymentCommand = array(
        "eshopGoId" => (float) $eshopGoId,
        "productName" => trim($productName),
        "totalPrice" => (int) $totalPriceInCents,
        "variableSymbol" => trim($variableSymbol),
        "successURL" => trim($successURL),
        "failedURL" => trim($failedURL),
        "encryptedSignature" => $encryptedSignature,
        "customerData" => $customerData,
        "paymentChannels" => join($paymentChannels, ",")
      );
      
      /*
       * Vytvareni platby na strane GoPay prostrednictvim WS
       */
      $payment_status = $go_client->__call('createCustomerPaymentSession', array(
        'paymentCommand' => $customerPaymentCommand
      ));
      
      /*
       * Kontrola stavu platby - musi byt ve stavu WAITING, kontrola parametru platby
       */
      if (GopayHelper::checkEshopPaymentResult($payment_status, 'WAITING', (float) $eshopGoId, $variableSymbol, (int) $totalPriceInCents, $productName, $secret)) {
        return $payment_status->paymentSessionId;
      } else {
        /*
         * Chyba pri vytvareni platby
         */
        return -1;
      }
    }
    catch (SoapFault $f) {
      /*
       * Chyba pri komunikaci s WS
       */
      return -2;
    }
  }
  
  /**
   * Kontrola stavu platby eshopu
   * - verifikace parametru z redirectu
   * - kontrola stavu platby
   *
   * @param long $paymentSessionId - identifikator platby
   * @param long $eshopGoId - identifikator eshopu - GoId
   * @param string $variableSymbol - identifikator objednavky v eshopu
   * @param int $totalPriceInCents - celkova cena objednavky v halerich
   * @param string $productName - popis objednavky zobrazujici se na platebni brane
   * @param string $secret - kryptovaci heslo pridelene eshopu
   *
   * @return $result
   *  result["code"]          - kod vysledku volani
   *  result["description"] - popis vysledku volani
   */
  public static function isEshopPaymentDone($paymentSessionId, $eshopGoId, $variableSymbol, $totalPriceInCents, $productName, $secret) {
    $result = array();
    
    try {
      
      /*
       * Inicializace WS
       */
      ini_set("soap.wsdl_cache_enabled", "0");
      $go_client = new SoapClient(GopayConfig::ws(), array());
      
      /*
       * Sestaveni dotazu na stav platby
       */
      $sessionEncryptedSignature = GopayHelper::encrypt(GopayHelper::hash(GopayHelper::concatPaymentSession((float) $eshopGoId, (float) $paymentSessionId, $secret)), $secret);
      
      $payment_session = array(
        "eshopGoId" => (float) $eshopGoId,
        "paymentSessionId" => (float) $paymentSessionId,
        "encryptedSignature" => $sessionEncryptedSignature
      );
      
      /*
       * Kontrola stavu platby na strane GoPay prostrednictvim WS
       */
      $payment_status = $go_client->__call('paymentStatusGW2', array(
        'paymentSessionInfo' => $payment_session
      ));
      
      $result["description"] = $payment_status->resultDescription;
      $result["code"]        = $payment_status->sessionState;
      
      /*
       * Kontrola zaplacenosti objednavky, verifikace parametru objednavky
       */
      if (($result["code"] == GopayHelper::PAYMENT_DONE && !GopayHelper::checkEshopPaymentStatus($payment_status, 'PAYMENT_DONE', (float) $eshopGoId, $variableSymbol, (int) $totalPriceInCents, $productName, $secret)) || ($result["code"] == GopayHelper::WAITING && !GopayHelper::checkEshopPaymentStatus($payment_status, 'WAITING', (float) $eshopGoId, $variableSymbol, (int) $totalPriceInCents, $productName, $secret)) || ($result["code"] == GopayHelper::TIMEOUTED && !GopayHelper::checkEshopPaymentStatus($payment_status, 'TIMEOUTED', (float) $eshopGoId, $variableSymbol, (int) $totalPriceInCents, $productName, $secret)) || ($result["code"] == GopayHelper::CANCELED && !GopayHelper::checkEshopPaymentStatus($payment_status, 'CANCELED', (float) $eshopGoId, $variableSymbol, (int) $totalPriceInCents, $productName, $secret))) {
        /*
         * Platba nesprobehla korektne
         */
        $result["code"] = GopayHelper::FAILED;
      }
    }
    catch (SoapFault $f) {
      /*
       * Chyba v komunikaci s GoPay serverem
       */
      $result["code"]        = GopayHelper::FAILED;
      $result["description"] = GopayHelper::FAILED;
    }
    
    return $result;
  }
  
  //  ---      UZIVATEL   ----
  
  /**
   * Vytvoreni platby uzivatele pomoci WS
   *
   * @param long $buyerGoId - identifikator uzivatele - GoId
   * @param string $productName - popis objednavky zobrazujici se na platebni brane
   * @param int $totalPriceInCents - celkova cena objednavky v halerich
   * @param string $variableSymbol - identifikator objednavky
   * @param string $successURL - URL stranky, kam je zakaznik presmerovan po uspesnem zaplaceni
   * @param string $failedURL - URL stranky, kam je zakaznik presmerovan po zruseni platby / neuspesnem zaplaceni
   * @param string $secret - kryptovaci heslo pridelene uzivateli
   *
   * @return paymentSessionId
   * @return -1 vytvoreni platby neprobehlo uspesne
   * @return -2 chyba komunikace WS
   */
  public static function createBuyerPayment($buyerGoId, $productName, $totalPriceInCents, $variableSymbol, $successURL, $failedURL, $secret) {
    try {
      
      ini_set("soap.wsdl_cache_enabled", "0");
      $go_client = new SoapClient(GopayConfig::ws(), array());
      
      /*
       * Sestaveni pozadavku pro zalozeni platby
       */
      $encryptedSignature = GopayHelper::encrypt(GopayHelper::hash(GopayHelper::concatPaymentCommand((float) $buyerGoId, $productName, (int) $totalPriceInCents, $variableSymbol, $failedURL, $successURL, $secret)), $secret);
      
      $payment_command = array(
        "buyerGoId" => (float) $buyerGoId,
        "productName" => trim($productName),
        "totalPrice" => (int) $totalPriceInCents,
        "variableSymbol" => trim($variableSymbol),
        "successURL" => trim($successURL),
        "failedURL" => trim($failedURL),
        "encryptedSignature" => $encryptedSignature
      );
      
      /*
       * Vytvareni platby na strane GoPay prostrednictvim WS
       */
      $payment_status = $go_client->__call('createPaymentSession', array(
        'paymentCommand' => $payment_command
      ));
      
      /*
       * Kontrola stavu platby - musi byt ve stavu WAITING, kontrola parametru platby
       */
      if (GopayHelper::checkBuyerPaymentResult($payment_status, 'WAITING', (float) $buyerGoId, $variableSymbol, (int) $totalPriceInCents, $productName, $secret)) {
        return $payment_status->paymentSessionId;
      } else {
        /*
         * Chyba pri vytvareni platby
         */
        return -1;
      }
    }
    catch (SoapFault $f) {
      /*
       * Chyba pri komunikaci s WS
       */
      return -2;
    }
  }
  
  /**
   * Kontrola provedeni platby uzivatele
   * - verifikace parametru z redirectu
   * - kontrola provedeni platby
   *
   * @param long $paymentSessionId - identifikator platby
   * @param long $eshopGoId - identifikator uzivatele - GoId
   * @param string $variableSymbol - identifikator objednavky
   * @param int $totalPriceInCents - celkova cena objednavky v halerich
   * @param string $productName - popis objednavky zobrazujici se na platebni brane
   * @param string $secret - kryptovaci heslo pridelene uzivateli
   *
   * @return $result
   *  result["code"]          - kod vysledku volani
   *  result["description"] - popis vysledku volani
   */
  public static function isBuyerPaymentDone($paymentSessionId, $buyerGoId, $variableSymbol, $totalPriceInCents, $productName, $secret) {
    $result = array();
    
    try {
      
      //inicializace WS
      ini_set("soap.wsdl_cache_enabled", "0");
      $go_client = new SoapClient(GopayConfig::ws(), array());
      
      //sestaveni dotazu na stav platby
      $sessionEncryptedSignature = GopayHelper::encrypt(GopayHelper::hash(GopayHelper::concatPaymentSession((float) $buyerGoId, (float) $paymentSessionId, $secret)), $secret);
      
      $payment_session = array(
        "buyerGoId" => (float) $buyerGoId,
        "paymentSessionId" => (float) $paymentSessionId,
        "encryptedSignature" => $sessionEncryptedSignature
      );
      
      /*
       * Kontrola stavu platby na strane GoPay prostrednictvim WS
       */
      $payment_status = $go_client->__call('paymentStatusGW2', array(
        'paymentSessionInfo' => $payment_session
      ));
      
      $result["description"] = $payment_status->resultDescription;
      $result["code"]        = $payment_status->sessionState;
      
      /*
       * Kontrola zaplacenosti objednavky, verifikace parametru objednavky
       */
      if (!GopayHelper::checkBuyerPaymentStatus($payment_status, 'PAYMENT_DONE', (float) $buyerGoId, $variableSymbol, (int) $totalPriceInCents, $productName, $secret)) {
        /*
         * Platba neprobehla korektne
         */
        $result["code"] = GopayHelper::FAILED;
      }
    }
    catch (SoapFault $f) {
      /*
       * Chyba v komunikaci s GoPay serverem
       */
      $result["code"]        = GopayHelper::FAILED;
      $result["description"] = GopayHelper::FAILED;
    }
    
    return $result;
  }
  
  public static function paymentMethodList() {
    try {
      
      //inicializace WS
      ini_set("soap.wsdl_cache_enabled", "0");
      $go_client = new SoapClient(GopayConfig::ws(), array());
      
      $paymentMethodsWS = $go_client->__call("paymentMethodList", array());
      
      $paymentMethods = new PaymentMethods();
      $paymentMethods->adapt($paymentMethodsWS);
      
      return $paymentMethods->paymentMethods;
    }
    catch (SoapFault $f) {
      dpm($f, 'paymentMethodList - SoapFault');
      /*
       * Chyba v komunikaci s GoPay serverem
       */
      return null;
    }
  }
  
}


/**
 * Definice platebnich metod - stazeno pomoci WS ze serveru GoPay
 */
class PaymentMethods {
  
  var $paymentMethods = array();
  
  public function adapt($paymentMethodsWS) {
    $this->paymentMethods = $paymentMethodsWS;
  }
  
}

class PaymentMethodElement {
  var $code = null;
  var $paymentMethodName = null;
  var $description = null;
  var $logo = null;
  var $offline = null;
  
  function PaymentMethodElement($code, $paymentMethodName, $description, $logo, $offline) {
    $this->code              = $code;
    $this->paymentMethodName = $paymentMethodName;
    $this->description       = $description;
    $this->logo              = $logo;
    $this->offline           = $offline;
  }
  
}

?>